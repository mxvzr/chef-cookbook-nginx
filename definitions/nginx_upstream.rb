define :nginx_upstream, :enable => true, :timing => :delayed do
    # create/update the configuration file
    template "#{node['nginx']['dir']}/sites-available/_#{params[:name]}" do
        source "upstream.erb"
        variables(params)
    end
    # enable/disable the upstream
    nginx_site "_#{params[:name]}" do
        enable params[:enable]
        timing params[:timing]
    end
end